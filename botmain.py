import os
import logging
import discord
from discord.ext import commands
from os.path import join, dirname
from dotenv import load_dotenv

# import calcFunctions
import music
import chat

# Enable logging
logger = logging.getLogger("discord")
logger.setLevel(logging.ERROR)
# logging.basicConfig(level=logging.CRITICAL)
dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)
TOKEN = os.environ.get("TOKEN")

commandKey = "`"

# Code based on
# https://github.com/Rapptz/discord.py/blob/master/examples/basic_voice.py

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(
    commands.when_mentioned_or(commandKey),
    description="Music and maybe reminder bot...",
    intents=intents,
)


@bot.event
async def on_ready():
    print(f"Logged in as {bot.user} (ID: {bot.user.id})")
    await bot.add_cog(chat.Chat(bot))
    await bot.add_cog(music.Music(bot))


bot.run(TOKEN)
