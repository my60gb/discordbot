# Adds ints from arglist and returns result
def add(arglist):
    answer = 0
    for i in range(1, len(arglist)):
        try:
            int(arglist[i])
            answer += int(arglist[i])
        except ValueError:
            return arglist[i] + " ain't a number bro"
    return answer


# TODO Meant to allow writing commands more naturally
# eg. $calc 4+5 instead of $add 4 5


def calc(arglist):
    if len(arglist) % 2 == 0:
        return "Woo"
    else:
        return "That ain't right"
